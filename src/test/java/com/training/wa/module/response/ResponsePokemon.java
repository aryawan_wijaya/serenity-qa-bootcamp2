package com.training.wa.module.response;

import java.util.List;

public class ResponsePokemon {
    private String name;
    private String id;
    private double height;
    private double weight;
    private List<ResponsePokemonDataType> types;
    private ResponsePokemonSpecies species;

    public ResponsePokemonSpecies getSpecies() {
        return species;
    }

    public void setSpecies(ResponsePokemonSpecies species) {
        this.species = species;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public List<ResponsePokemonDataType> getTypes() {
        return types;
    }

    public void setTypes(List<ResponsePokemonDataType> types) {
        this.types = types;
    }
}
