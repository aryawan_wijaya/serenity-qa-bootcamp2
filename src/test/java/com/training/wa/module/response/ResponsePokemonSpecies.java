package com.training.wa.module.response;

public class ResponsePokemonSpecies {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
