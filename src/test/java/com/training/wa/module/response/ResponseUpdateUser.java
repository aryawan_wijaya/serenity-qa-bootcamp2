package com.training.wa.module.response;

public class ResponseUpdateUser {
    private int code;
    private String status;
    private ResponseUpdateUserData data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ResponseUpdateUserData getData() {
        return data;
    }

    public void setData(ResponseUpdateUserData data) {
        this.data = data;
    }
}
