package com.training.wa.module.response;

public class ResponseSpeciesGeneraGenus {
    private String genus;

    public String getGenus() {
        return genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }
}
