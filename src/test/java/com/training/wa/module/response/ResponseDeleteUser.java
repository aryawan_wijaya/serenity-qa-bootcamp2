package com.training.wa.module.response;

public class ResponseDeleteUser {
    private String code;
    private String status;
    private ResponseGetUserData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ResponseGetUserData getData() {
        return data;
    }

    public void setData(ResponseGetUserData data) {
        this.data = data;
    }
}
