package com.training.wa.module.response;

import java.util.List;

public class ResponseSpeciesGenera {
    private List<ResponseSpeciesGeneraGenus> genera;

    public List<ResponseSpeciesGeneraGenus> getGenera() {
        return genera;
    }

    public void setGenera(List<ResponseSpeciesGeneraGenus> genera) {
        this.genera = genera;
    }
}
