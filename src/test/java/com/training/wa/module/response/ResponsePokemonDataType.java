package com.training.wa.module.response;

public class ResponsePokemonDataType {


    private ResponsePokemonDataTypeName type;
    public ResponsePokemonDataTypeName getResponsePokemonDataTypeName() {
        return type;
    }

    public void setResponsePokemonDataTypeName(ResponsePokemonDataTypeName responsePokemonDataTypeName) {
        this.type = responsePokemonDataTypeName;
    }
}
