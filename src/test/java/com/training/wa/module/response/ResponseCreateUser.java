package com.training.wa.module.response;

public class ResponseCreateUser {
        private int code;
        private ResponseCreateUserData data;
        private String status;

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public int getCode() {
                return code;
        }

        public void setCode(int code) {
                this.code = code;
        }

        public ResponseCreateUserData getData() {
                return data;
        }

        public void setData(ResponseCreateUserData data) {
                this.data = data;
        }
}
