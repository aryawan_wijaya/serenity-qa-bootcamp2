package com.training.wa.module;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/consult_dictionary/"
        ,tags = "@APIUiPokemon and @CekUiAPIPokemon")
public class DefinitionTestSuite {}
