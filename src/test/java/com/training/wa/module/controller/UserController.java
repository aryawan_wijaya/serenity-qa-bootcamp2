package com.training.wa.module.controller;

import static net.serenitybdd.rest.RestRequests.given;

import com.training.wa.module.Request.RequestCreateUser;
import com.training.wa.module.Request.RequestUpdateUser;
import com.training.wa.module.data.UserData;
import io.restassured.response.Response;
import org.seleniumhq.jetty9.server.Authentication;

public class UserController {
    public static Response getUser () {
        return  given().
                queryParam("id",UserData.getId()).
                header("Accept", "application/json").
                when().
                get("http://localhost:17001/api/member");
    }

    public static Response createUser() {
        RequestCreateUser request = new RequestCreateUser();
        request.setEmail(UserData.getEmail());
        request.setName(UserData.getName());
        request.setPhoneNumber(UserData.getPhoneNumber());
        return given().
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                body(request).
                when().
                post("http://localhost:17001/api/member");
    }
    public static Response updateUser(){
        RequestUpdateUser request = new RequestUpdateUser();
        request.setEmail(UserData.getEmail());
        request.setName(UserData.getName());
        request.setPhoneNumber(UserData.getPhoneNumber());
        return given().
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                queryParam("id",UserData.getId()).
                body(request).
                when().
                put("http://localhost:17001/api/member");
    }
    public static Response deleteUser(){
        return  given().
                queryParam("id",UserData.getId()).
                header("Accept", "application/json").
                when().
                delete("http://localhost:17001/api/member");
    }

}
