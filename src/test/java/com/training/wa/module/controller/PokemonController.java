package com.training.wa.module.controller;

import com.training.wa.module.data.PokemonDataAPI;
import com.training.wa.module.data.UserData;
import com.training.wa.module.response.ResponsePokemonSpecies;
import com.training.wa.module.steps.Pokemon;
import io.restassured.response.Response;

import static net.serenitybdd.rest.RestRequests.given;

public class PokemonController {
    public static Response getPokemon () {
        return  given().
                header("Accept", "application/json").
                when().
                get("https://pokeapi.co/api/v2/pokemon/"+ PokemonDataAPI.getName());
    }
    public static Response getSpeciesApi(String url){
        return given().
                header("Accept", "application/json").
                when().
                get(url);
    }
}
