package com.training.wa.module.data;

import io.restassured.response.Response;

public class PokemonDataAPI {
    private static String name;
    private static int id;
    private static double height;
    private static double weight;
    private static String url;
    private static Response responsePokemon;
    private static Response responseSpeciesGenera;

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        PokemonDataAPI.url = url;
    }



    public static Response getResponseSpeciesGenera() {
        return responseSpeciesGenera;
    }

    public static void setResponseSpeciesGenera(Response responseSpeciesGenera) {
        PokemonDataAPI.responseSpeciesGenera = responseSpeciesGenera;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        PokemonDataAPI.name = name;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        PokemonDataAPI.id = id;
    }

    public static double getHeight() {
        return height;
    }

    public static void setHeight(double height) {
        PokemonDataAPI.height = height;
    }

    public static double getWeight() {
        return weight;
    }

    public static void setWeight(double weight) {
        PokemonDataAPI.weight = weight;
    }

    public static Response getResponsePokemon() {
        return responsePokemon;
    }

    public static void setResponsePokemon(Response responsePokemon) {
        PokemonDataAPI.responsePokemon = responsePokemon;
    }
}
