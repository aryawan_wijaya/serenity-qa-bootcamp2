package com.training.wa.module.data;

import io.restassured.response.Response;

public class UserData {
    private static int id;
    private static String name;
    private static String email;
    private static String phoneNumber;

    private static Response responseCreateUser;
    private static Response responseUpdateUser;
    private static Response responseGetUser;
    private static Response responseDeleteUser;
    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        UserData.id = id;
    }
    public static Response getResponseDeleteUser() {
        return responseDeleteUser;
    }

    public static void setResponseDeleteUser(Response responseDeleteUser) {
        UserData.responseDeleteUser = responseDeleteUser;
    }

    public static Response getResponseGetUser() {
        return responseGetUser;
    }

    public static void setResponseGetUser(Response responseGetUser) {
        UserData.responseGetUser = responseGetUser;
    }

    public static Response getResponseUpdateUser() {
        return responseUpdateUser;
    }

    public static void setResponseUpdateUser(Response responseUpdateUser) {
        UserData.responseUpdateUser = responseUpdateUser;
    }

    public static Response getResponseCreateUser() {
        return responseCreateUser;
    }

    public static void setResponseCreateUser(Response responseCreateUser) {
        UserData.responseCreateUser = responseCreateUser;
    }



    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        UserData.name = name;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        UserData.email = email;
    }

    public static String getPhoneNumber() {
        return phoneNumber;
    }

    public static void setPhoneNumber(String phoneNumber) {
        UserData.phoneNumber = phoneNumber;
    }
}
