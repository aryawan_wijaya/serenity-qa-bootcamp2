package com.training.wa.module.data;

public class PokemonData {
    private static String number;
    private static String name;
    private static  String [] type;
    private static String species;
    private static double height;
    private static double weight;

    public static String[] getType() {
        return type;
    }

    public static void setType(String[] type) {
        PokemonData.type = type;
    }

    public static String getNumber() {
        return number;
    }

    public static void setNumber(String number) {
        PokemonData.number = number;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        PokemonData.name = name;
    }

    public static String getSpecies() {
        return species;
    }

    public static void setSpecies(String species) {
        PokemonData.species = species;
    }

    public static double getHeight() {
        return height;
    }

    public static void setHeight(double height) {
        PokemonData.height = height;
    }

    public static double getWeight() {
        return weight;
    }

    public static void setWeight(double weight) {
        PokemonData.weight = weight;
    }
}
