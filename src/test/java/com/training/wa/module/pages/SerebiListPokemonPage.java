package com.training.wa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.actions.Switch;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.*;

@DefaultUrl("https://www.serebii.net/fireredleafgreen/kantopokedex.shtml")
public class SerebiListPokemonPage extends PageObject {
    private static String POKEMON_PATH = "//a[contains(text(),'${pokemon}')]";
    private static String POKEMON_LIST_PATH =".//td[@class='fooinfo'][3]";
    private static String POKEMON_TYPE_LIST ="(.//td[@class='tooltabcon'])[4]//child::img";

    @FindBy(xpath = "(.//td[@class='tooltabcon'])[1]")
    private WebElementFacade namePokemon;

    @FindBy(xpath = "(.//td[@class='tooltabcon'])[6]")
    private WebElementFacade heightPokemon;

    @FindBy(xpath = "(.//td[@class='tooltabcon'])[7]")
    private WebElementFacade weightPokemon;

    @FindBy(xpath = "(.//td[@class='tooltabcon'])[5]")
    private WebElementFacade speciesPokemon;

    @FindBy(xpath = "(.//b[contains(text(),'National')]//following::td)[1]")
    private WebElementFacade numberPokemon;
    public void choosePokemon (String name){
        WebElementFacade selectItem = find(By.xpath(POKEMON_PATH.replace("${pokemon}",name)));
        selectItem.click();
    }

    public List<String> getListPokemon (){
        List<WebElementFacade> listPokemon = findAll(By.xpath(POKEMON_LIST_PATH));
        List <String> pokemonText = new ArrayList<>();
        for(WebElementFacade pokemon : listPokemon){
            pokemonText.add(pokemon.getText());
        }
        return pokemonText;
    }
    public String getNamePokemon(){
        System.out.println(namePokemon.getText());
        return namePokemon.getText().trim();
    }
    public String getNumberPokemon(){
        String number = numberPokemon.getText().substring(1);
        System.out.println(number);
        return number.trim();
    }
    public String getSpeciesPokemon(){
        System.out.println(speciesPokemon.getText());
        return speciesPokemon.getText().trim();
    }
    public double getHeightPokemon(){
        String height = heightPokemon.getText();
        String subStr = height.substring(height.indexOf("”")+1,height.indexOf("m"));
        double value = Double.parseDouble(subStr);
        System.out.println(value);
        return value;
    }
    public double getWeightPokemon(){
        String weight = weightPokemon.getText();
        String subStr = weight.substring(weight.indexOf("s")+1,weight.indexOf("k"));
        double value = Double.parseDouble(subStr);
        System.out.println(value);
        return value;
    }

    public String [] getTypePokemon (){
        List<WebElementFacade> listLink = findAll(By.xpath(POKEMON_TYPE_LIST));
        System.out.println(listLink.size());
        String [] type = new String[listLink.size()];
        for(int i =0;i<listLink.size();i++){
            type[i] = POKEMON_LIST.get(listLink.get(i).getAttribute("src")).trim();
            System.out.println(type[i]);
        }
        return type;
    }
    public static final Map<String,String> POKEMON_LIST;
    static {
        Map<String,String> map=new HashMap<>();
        map.put("https://www.serebii.net/pokedex-bw/type/bug.gif","BUG");
        map.put("https://www.serebii.net/pokedex-bw/type/poison.gif","POISON");
        map.put("https://www.serebii.net/pokedex-bw/type/fire.gif","FIRE");
        map.put("https://www.serebii.net/pokedex-bw/type/flying.gif","FLYING");
        map.put("https://www.serebii.net/pokedex-bw/type/water.gif","WATER");
        map.put("https://www.serebii.net/pokedex-bw/type/electric.gif","ELECTRIC");
        map.put("https://www.serebii.net/pokedex-bw/type/ground.gif","GROUND");
        map.put("https://www.serebii.net/pokedex-bw/type/grass.gif","GRASS");
        map.put("https://www.serebii.net/pokedex-bw/type/fighting.gif","FIGHTING");
        POKEMON_LIST= Collections.unmodifiableMap(map);
    }
}
