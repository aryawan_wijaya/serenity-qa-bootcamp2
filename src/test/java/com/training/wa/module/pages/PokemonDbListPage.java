package com.training.wa.module.pages;

import cucumber.api.java.eo.Do;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

@DefaultUrl("https://pokemondb.net/pokedex/game/red-blue-yellow")
public class PokemonDbListPage extends PageObject {
    private static String POKEMON_LIST_XPATH = ".//a[@class='ent-name']";

    @FindBy(xpath = ".//main[@class='main-content grid-container']//following::h1")
    private WebElementFacade namePokemon;

    @FindBy(xpath = ".//table[@class='vitals-table']//following::strong")
    private WebElementFacade numberPokemon;

    @FindBy(xpath = ".//th[contains(text(),'Species')]//following::td[1]")
    private WebElementFacade speciesPokemon;

    @FindBy(xpath = "(.//th[contains(text(),'Type')]//following::td[1])[1]")
    private WebElementFacade typePokemon;

    @FindBy(xpath = "(.//th[contains(text(),'Height')]//following::td[1])[1]")
    private WebElementFacade heightPokemon;

    @FindBy(xpath = "(.//th[contains(text(),'Weight')]//following::td[1])[1]")
    private WebElementFacade weightPokemon;

    public List<String> listPokemon(){
        List<WebElementFacade> listPokemon = findAll(By.xpath(POKEMON_LIST_XPATH));
        List <String> pokemonText = new ArrayList<>();
        for(WebElementFacade pokemon : listPokemon){
            pokemonText.add(pokemon.getText().toLowerCase());
        }
        return pokemonText;
    }
    public void choosePokemon(String name){
        List<WebElementFacade> listPokemon = findAll(By.xpath(POKEMON_LIST_XPATH));
        for(WebElementFacade pokemon :listPokemon){
            if(pokemon.getText().toLowerCase().contains(name.toLowerCase())){
                pokemon.click();
                break;
            }
        }
    }
    public String getNamePokemon(){
//        System.out.println(namePokemon.getText());
        return namePokemon.getText();
    }
    public String getNumberPokemon(){
//        System.out.println(numberPokemon.getText());
        String number = numberPokemon.getText();
        String subStr = number.substring(number.indexOf("0")+1);
        return subStr.trim();
    }
    public String getSpeciesPokemon(){
        System.out.println(speciesPokemon.getText());
        return speciesPokemon.getText();
    }
    public double getHeight(){
        String height= heightPokemon.getText();
        String subStr = height.substring(0,height.indexOf(" "));
        double value = Double.parseDouble(subStr);
//        System.out.println(value);
        return value;
    }
    public double getWeight(){
        String weight = weightPokemon.getText();
        String subStr = weight.substring(0,weight.indexOf(" "));
        double value = Double.parseDouble(subStr);
//        System.out.println(value);
        return value;
    }
    public String [] getTypePokemon(){
        String []type = typePokemon.getText().toLowerCase().split(" ");
        return type;
    }
}
