package com.training.wa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

@DefaultUrl("https://www.seleniumeasy.com/test/input-form-demo.html")
public class SeleniumTrainingDataTable  extends PageObject {
    @FindBy(name = "first_name")
    private WebElementFacade firstName;

    @FindBy(name = "last_name")
    private WebElementFacade lastName;

    @FindBy(name="email")
    private WebElementFacade email;

    @FindBy(name = "phone")
    private WebElementFacade phone;

    @FindBy(name="address")
    private WebElementFacade address;

    @FindBy(name="city")
    private WebElementFacade city;

    @FindBy(name="state")
    private WebElementFacade state;

    @FindBy(name="zip")
    private WebElementFacade zip;

    @FindBy(name = "website")
    private WebElementFacade website;

    @FindBy(xpath = "(.//input[@type='radio'])[1]")
    private WebElementFacade hostingYes;

    @FindBy(xpath = "(.//input[@type='radio'])[2]")
    private WebElementFacade hostingNo;

    @FindBy (name = "comment")
    private WebElementFacade projectDesc;


    public void typeFirstName(String input){
        firstName.type(input);
    }
    public void typeLastName(String input){
        lastName.type(input);
    }
    public void typeEmail(String input){
        email.type(input);
    }
    public void typePhone(String input){
        phone.type(input);
    }
    public void typeAddress(String input){
        address.type(input);
    }
    public void typeCity(String input){
        city.type(input);
    }
    public void chooseState(String input){
        state.click();
        Select select = new Select (state);
        select.selectByVisibleText(input);
    }
    public void typeZipeCode(String input){
        zip.type(input);
    }
    public void typeWebsite(String input){
        website.type(input);
    }
    public void chooseHosting(boolean input){
        System.out.println(input);
        if(input){
            hostingYes.click();
        }else{
            hostingNo.click();
        }
    }
    public void typeProjectDesc(String input){
        projectDesc.type(input);
    }

}
