package com.training.wa.module.pages;

import com.sun.xml.bind.v2.runtime.unmarshaller.TagName;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class WikipediaPage extends PageObject {
    @FindBy(tagName="body")
    private WebElementFacade bodyWikipedia;

    public String getBodyText(){
        return bodyWikipedia.getText();
    }

}
