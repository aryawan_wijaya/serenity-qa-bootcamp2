package com.training.wa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://duckduckgo.com/")
public class DuckDuckGoPage extends PageObject {
    @FindBy(name="q")
    private WebElementFacade txtSearch;

    @FindBy(xpath = "(.//a[@class='result__a'])[1]")
    private WebElementFacade txtBlibliWikipedia; //webElementFacade adanya di serenity jadi @FindBy juga harus dari serenity
    // kegunaanya dia lebih lengkap dari selenim bahkan bisa untuk run di android juga, dan banyak keunggulan spt typeAndEnter

    public void searching(String keyword){
        txtSearch.typeAndEnter(keyword);
    }

    public void clickBlibliWikipedia(){
        txtBlibliWikipedia.click();
    }

}
