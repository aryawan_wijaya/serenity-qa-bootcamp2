package com.training.wa.module.steps;

import com.training.wa.module.controller.UserController;
import com.training.wa.module.data.UserData;

import com.training.wa.module.response.ResponseCreateUser;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class APICreateStep {

    @Given("^user set email to \"([^\"]*)\"$")
    public void userSetEmailTo(String valeue) throws Throwable {
        UserData.setEmail(valeue);
    }

    @And("^set name to \"([^\"]*)\"$")
    public void setNameTo(String value) throws Throwable {
        UserData.setName(value);
    }
    @And("^set phone to \"([^\"]*)\"$")
    public void setPhoneTo(String value) throws Throwable {
        UserData.setPhoneNumber(value);
    }

    @When("^user send request$")
    public void userSendRequest() {
        UserData.setResponseCreateUser(UserController.createUser());
    }
    @Then("^create user status code must be (\\d+)$")
    public void createUserStatusCodeMustBe(int code) {
        assertThat("Response code create user is not valid",UserData.getResponseCreateUser().statusCode(),equalTo(code));
    }

    @And("^the data from user must equal with request$")
    public void theDataFromUserMustEqualWithRequest() {
        ResponseCreateUser responseCreateUser = UserData.getResponseCreateUser().getBody().as(ResponseCreateUser.class);
        assertThat("Email is not valid",responseCreateUser.getData().getEmail(),equalTo(UserData.getEmail()));
        assertThat("Name is not valid",responseCreateUser.getData().getName(),equalTo(UserData.getName()));
        assertThat("Phone is not valid",responseCreateUser.getData().getPhoneNumber(),equalTo(UserData.getPhoneNumber()));
    }



}
