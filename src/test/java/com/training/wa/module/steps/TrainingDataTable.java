package com.training.wa.module.steps;

import com.training.wa.module.pages.SeleniumTrainingDataTable;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import javafx.beans.binding.ObjectExpression;
import jnr.ffi.Struct;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class TrainingDataTable {
    SeleniumTrainingDataTable trainingDataTable;
    @Given("^user open form$")
    public void userOpenForm() {
        trainingDataTable.open();
    }

    @When("^user fill the form with following data$")
    public void userFillTheFormWithFollowingData(DataTable formDataTable) {
        Map<String,Object >formData = formDataTable.asMap(String.class,Object.class);
        trainingDataTable.typeFirstName(formData.get("firstName").toString());
        trainingDataTable.typeLastName(formData.get("lastName").toString());
        trainingDataTable.typeEmail(formData.get("email").toString());
        trainingDataTable.typePhone(formData.get("phone").toString());
        trainingDataTable.typeAddress(formData.get("address").toString());
        trainingDataTable.typeCity(formData.get("city").toString());
        trainingDataTable.chooseState(formData.get("state").toString().trim());
        trainingDataTable.typeZipeCode(formData.get("zipCode").toString());
        trainingDataTable.typeWebsite(formData.get("website").toString());
        trainingDataTable.chooseHosting(Boolean.valueOf(formData.get("isHosting").toString()));
        trainingDataTable.typeProjectDesc(formData.get("projectDescription").toString());
       /* Map<String,Object> formData=formDataTable.asMap(String.class,Object.class);
        //String krn kita ingin key nya dalam bentuk String, dan object krn inputan data tidak mesti string bisa angka atau boolean
        String value =formData.get("firstName").toString();
        System.out.println(value);
        Boolean isExist = formData.containsKey("cona"); //utk cek key nya ada atau tidak
        System.out.println(isExist);*/
        //--------------------buat map baru, tidak ada hubungannya dengan data table yang ada di feature----------
      /*  Map<String,Object>newFormData = new HashMap<>();
        System.out.println(newFormData);
        newFormData.put("firstName2","Blibli"); //memasukan data di map baru  dengan key firstName
        newFormData.put("lastName2","Dot Com");
        System.out.println(newFormData);
        newFormData.remove("firstName2");
        newFormData.put("lastName2",".Com");
        System.out.println(newFormData);*/

        //-------------------coba scenario outline untuk scenario ke dua-------------------------------
     /*   Map<String, Object>formData = formDataTable.asMap(String.class, Object.class);
        trainingDataTable.typeFirstName(formData.get("firstName").toString());
        trainingDataTable.typeLastName(formData.get("lastName").toString());
        trainingDataTable.typeEmail(formData.get("email").toString());*/
    }

    @When("^user fill firstname with value '(.*)'$")
    public void userFillFirstnameWithValueFirstName(String firstName) {
        trainingDataTable.typeFirstName(firstName);
    }

    @And("^user fill  lastname with value '(.*)'$")
    public void userFillLastnameWithValueLastName(String lastName) {
        trainingDataTable.typeLastName(lastName);
    }

    @And("^user fill  email with value '(.*)'$")
    public void userFillEmailWithValueEmail(String email) {
        trainingDataTable.typeEmail(email);
    }



    @Then("^validated url with \"([^\"]*)\"$")
    public void validatedUrlWith(String expectedCondition) throws Throwable {
        String url= trainingDataTable.getDriver().getCurrentUrl();
        assertThat("Invalid URL",url,equalTo(expectedCondition));
    }
}
