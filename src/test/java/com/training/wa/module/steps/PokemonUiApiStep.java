package com.training.wa.module.steps;

import com.training.wa.module.controller.PokemonController;
import com.training.wa.module.data.PokemonData;
import com.training.wa.module.data.PokemonDataAPI;
import com.training.wa.module.pages.PokemonDbListPage;
import com.training.wa.module.response.ResponsePokemon;
import com.training.wa.module.response.ResponseSpeciesGenera;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PokemonUiApiStep {
    PokemonDbListPage pokemonDbListPage;
    @Given("^user open website pokemonDb$")
    public void userOpenWebsitePokemonDb() {
        pokemonDbListPage.open();
    }

    @Then("^validated \"([^\"]*)\" from website is display$")
    public void validatedFromWebsiteIsDisplay(String name) throws Throwable {
        assertThat("No pokemon display",pokemonDbListPage.listPokemon(),hasItems(name.toLowerCase()));
    }

    @When("^user click \"([^\"]*)\" from website$")
    public void userClickFromWebsite(String name) throws Throwable {
        pokemonDbListPage.choosePokemon(name);
    }

    @And("^user save the data from website$")
    public void userSaveTheDataFromWebsite() {
        PokemonData.setNumber(pokemonDbListPage.getNumberPokemon().toLowerCase());
        PokemonData.setName(pokemonDbListPage.getNamePokemon().toLowerCase());
        PokemonData.setSpecies(pokemonDbListPage.getSpeciesPokemon().toLowerCase());
        PokemonData.setHeight(pokemonDbListPage.getHeight());
        PokemonData.setWeight(pokemonDbListPage.getWeight());
        PokemonData.setType(pokemonDbListPage.getTypePokemon());
    }

    @And("^user set \"([^\"]*)\" api$")
    public void userSetApi(String name) throws Throwable {
        PokemonDataAPI.setName(name.toLowerCase());
    }

    @And("^user hit API$")
    public void userHitAPI() {
        PokemonDataAPI.setResponsePokemon(PokemonController.getPokemon());
    }

    @Then("^compare pokemon from website and API$")
    public void comparePokemonFromWebsiteAndAPI() {
        PokemonDataAPI.getResponsePokemon().getBody().prettyPrint();
        ResponsePokemon responsePokemon = PokemonDataAPI.getResponsePokemon().getBody().as(ResponsePokemon.class);
        assertThat("name of pokemon is not same",PokemonData.getName(),containsString(responsePokemon.getName()));
        assertThat("number of pokemon is not same",PokemonData.getNumber(),containsString(responsePokemon.getId()));
        assertThat("height of pokemon is not same",PokemonData.getHeight()*10,is(closeTo(responsePokemon.getHeight(),0.0)));
        assertThat("weight of pokemon is not same",PokemonData.getWeight()*10,is(closeTo(responsePokemon.getWeight(),0.0)));

       System.out.println(responsePokemon.getSpecies().getUrl());
       /*  System.out.println(PokemonData.getName());
        System.out.println(responsePokemon.getHeight());
        System.out.println(PokemonData.getHeight()*10);
        System.out.println(responsePokemon.getWeight());
        System.out.println(PokemonData.getWeight()*10);
        System.out.println(responsePokemon.getId());
        System.out.println(PokemonData.getNumber());
        System.out.println(responsePokemon.getTypes().get(0).getResponsePokemonDataTypeName().getName())

        for(int i =0;i<PokemonData.getType().length;i++){
            System.out.println(PokemonData.getType()[i]);
        }
        for(int i =0;i<responsePokemon.getTypes().size();i++){
            System.out.println(responsePokemon.getTypes().get(i).getResponsePokemonDataTypeName().getName().toLowerCase());
        }*/
        List<String> responPokemonTypeText = new ArrayList<>();
        for (int i =0;i<responsePokemon.getTypes().size();i++){
            responPokemonTypeText.add(responsePokemon.getTypes().get(i).getResponsePokemonDataTypeName().getName().toLowerCase());
        }
        assertThat("type is not same",responPokemonTypeText,containsInAnyOrder(PokemonData.getType()));
//        System.out.println(responsePokemon.getSpecies().getUrl());
        PokemonDataAPI.setUrl(responsePokemon.getSpecies().getUrl());
        PokemonDataAPI.setResponseSpeciesGenera( PokemonController.getSpeciesApi(PokemonDataAPI.getUrl()));

        ResponseSpeciesGenera responseSpeciesGenera =PokemonDataAPI.getResponseSpeciesGenera().getBody().as(ResponseSpeciesGenera.class);
        System.out.println(responseSpeciesGenera.getGenera().get(2).getGenus().toLowerCase());
        System.out.println(PokemonData.getSpecies());
        assertThat("Species is not same",PokemonData.getSpecies(),containsString(responseSpeciesGenera.getGenera().get(7).getGenus().toLowerCase()));


    }
}
