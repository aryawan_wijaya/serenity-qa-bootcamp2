package com.training.wa.module.steps;

import com.training.wa.module.data.PokemonData;
import com.training.wa.module.pages.PokemonDbListPage;
import com.training.wa.module.pages.SerebiListPokemonPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Pokemon {
    PokemonDbListPage pokemonDbListPage;
    SerebiListPokemonPage serebiListPokemonPage;

    @Given("^user open pokemonDb website$")
    public void userOpenPokemonDbWebsite() {
        pokemonDbListPage.open();
    }

    @Then("^validated that \"([^\"]*)\" is display$")
    public void validatedThatIsDisplay(String name) throws Throwable {
        assertThat("No pokemon display",pokemonDbListPage.listPokemon(),hasItems(name));
    }

    @When("^user click \"([^\"]*)\" pokemmon$")
    public void userClickPokemmon(String name) throws Throwable {
        pokemonDbListPage.choosePokemon(name);
    }


    @And("^user save the data$")
    public void userSaveTheData() {
        PokemonData.setNumber(pokemonDbListPage.getNumberPokemon());
        PokemonData.setName(pokemonDbListPage.getNamePokemon());
        PokemonData.setSpecies(pokemonDbListPage.getSpeciesPokemon());
        PokemonData.setHeight(pokemonDbListPage.getHeight());
        PokemonData.setWeight(pokemonDbListPage.getWeight());
        PokemonData.setType(pokemonDbListPage.getTypePokemon());




        for(int i=0; i<pokemonDbListPage.getTypePokemon().length;i++){
            System.out.println(pokemonDbListPage.getTypePokemon()[i]);
        }
        System.out.println("==================================================");
    }


    @And("^user open serebi website$")
    public void userOpenSerebiWebsite() {
        serebiListPokemonPage.open();
    }

    @Then("^validated second pokemon \"([^\"]*)\" is display$")
    public void validatedSecondPokemonIsDisplay(String name) throws Throwable {
        assertThat("No pokemon display",serebiListPokemonPage.getListPokemon(),hasItems(containsString(name)));
    }
    @When("^user click second pokemon \"([^\"]*)\" pokemmon$")
    public void userClickSecondPokemonPokemmon(String name) throws Throwable {
        serebiListPokemonPage.choosePokemon(name);
    }

    @Then("^compare pokemon data from the first website and the second website$")
    public void comparePokemonDataFromTheFirstWebsiteAndTheSecondWebsite() {
        assertThat("name is not same",PokemonData.getName(),equalTo(serebiListPokemonPage.getNamePokemon()));
        assertThat("number is not same",PokemonData.getNumber(),equalTo(serebiListPokemonPage.getNumberPokemon()));
        assertThat("species is not same",PokemonData.getSpecies(),equalTo(serebiListPokemonPage.getSpeciesPokemon()));
        assertThat("weight is not same",PokemonData.getWeight(),equalTo(serebiListPokemonPage.getWeightPokemon()));
        assertThat("height is not same",PokemonData.getHeight(),equalTo(serebiListPokemonPage.getHeightPokemon()));
        assertThat("type is not same",PokemonData.getType(),arrayContaining(serebiListPokemonPage.getTypePokemon()));
    }
}
