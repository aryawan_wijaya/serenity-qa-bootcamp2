package com.training.wa.module.steps;

import com.training.wa.module.controller.UserController;
import com.training.wa.module.data.UserData;
import com.training.wa.module.response.ResponseCreateUser;
import com.training.wa.module.response.ResponseUpdateUser;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.seleniumhq.jetty9.server.Authentication;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class APIUpdateStep {
    @Given("^\\[API\\] user set id (\\d+)$")
    public void apiUserSetId(int value) {
        UserData.setId(value);
    }

    @And("^\\[API\\] user set email \"([^\"]*)\"$")
    public void apiUserSetEmail(String value) throws Throwable {
        UserData.setEmail(value);
    }

    @And("^\\[API\\] user set name \"([^\"]*)\"$")
    public void apiUserSetName(String value) throws Throwable {
        UserData.setName(value);
    }

    @And("^\\[API\\] user set phone number \"([^\"]*)\"$")
    public void apiUserSetPhoneNumber(String value) throws Throwable {
        UserData.setPhoneNumber(value);
    }

    @When("^\\[API\\] user send request$")
    public void apiUserSendRequest() {
        UserData.setResponseUpdateUser(UserController.updateUser());
    }

    @Then("^\\[API\\] response code must be (\\d+)$")
    public void apiResponseCodeMustBe(int code) {
        assertThat("Response code create user is not valid",UserData.getResponseUpdateUser().statusCode(),equalTo(code));
    }

    @And("^\\[API\\] the data from user must equal with request$")
    public void apiTheDataFromUserMustEqualWithRequest() {
        ResponseUpdateUser responseUpdateUser = UserData.getResponseUpdateUser().getBody().as(ResponseUpdateUser.class);
        assertThat("Email is not valid",responseUpdateUser.getData().getEmail(),equalTo(UserData.getEmail()));
        assertThat("Name is not valid",responseUpdateUser.getData().getName(),equalTo(UserData.getName()));
        assertThat("Phone is not valid",responseUpdateUser.getData().getPhoneNumber(),equalTo(UserData.getPhoneNumber()));
    }


}
