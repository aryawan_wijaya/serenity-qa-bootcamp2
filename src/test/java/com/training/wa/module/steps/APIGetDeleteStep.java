package com.training.wa.module.steps;

import com.training.wa.module.controller.UserController;
import com.training.wa.module.data.UserData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.seleniumhq.jetty9.server.Authentication;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class APIGetDeleteStep {


    @Given("^\\[API\\] user set id get  (\\d+)$")
    public void apiUserSetIdGet(int id) {
        UserData.setId(id);
    }
    @When("^\\[API\\] user send request get$")
    public void apiUserSendRequestGet() {
        UserData.setResponseGetUser(UserController.getUser());
    }
    @Then("^\\[API\\] status code get must be (\\d+)$")
    public void apiStatusCodeGetMustBe(int code) {
        assertThat("Response code is not valid",UserData.getResponseGetUser().statusCode(),equalTo(code));
    }

    @When("^\\[API\\] user send request delete$")
    public void apiUserSendRequestDelete() {
        UserData.setResponseDeleteUser(UserController.deleteUser());
    }

    @Then("^\\[API\\] status code delete must be (\\d+)$")
    public void apiStatusCodeDeleteMustBe(int code) {
        assertThat("Response code is not valid",UserData.getResponseDeleteUser().statusCode(),equalTo(code));
    }
}
