package com.training.wa.module.steps;

import com.training.wa.module.pages.DuckDuckGoPage;
import com.training.wa.module.pages.WikipediaPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;

public class BlibliWikipediaSteps {
    private DuckDuckGoPage duckDuckGoPage;
    private WikipediaPage wikipediaPage;
    @Given("^user open duckduckgo home page$")
    public void userOpenDuckduckgoHomePage() {
        duckDuckGoPage.open();
    }

    @When("^user type \"([^\"]*)\" in search box$")
    public void userTypeInSearchBox(String keyword) throws Throwable {
//        throw new PendingException();//untuk memberhentikan code jika code kita blom kelar
        duckDuckGoPage.searching(keyword);
    }

    @And("^user click wikipedia link$")
    public void userClickWikipediaLink() {
        duckDuckGoPage.clickBlibliWikipedia();
    }


    @Then("^user should able to see \"([^\"]*)\"$")
    public void userShouldAbleToSee(String expected) throws Throwable {
        MatcherAssert.assertThat("Wikipedia is not opened",
                wikipediaPage.getBodyText(), Matchers.containsString(expected));

    }
}
