@API
Feature: Test create API

  @CreateUserPositive
  Scenario: create correct data in API
    Given   user set email to "tester3@gmail.com"
    And     set name to "testertest"
    And     set phone to "081232341"
    When    user send request
    Then    create user status code must be 200
    And     the data from user must equal with request
