@pokemon
Feature: Pokemon Check

  Scenario Outline: compare two website that contains pokemon database --> <test> --> with pokemon <pokemon>
    Given user open pokemonDb website
    Then validated that "<pokemon>" is display
    When user click "<pokemon>" pokemmon
    And user save the data
    And user open serebi website
    Then validated second pokemon "<pokemon>" is display
    When user click second pokemon "<pokemon>" pokemmon
    Then compare pokemon data from the first website and the second website

    Examples:
      | test  | pokemon   |
      | test1 | Weedle    |
      | test2 | Bulbasaur |
      | test3 | Venusaur  |
      | test4 | Raichu    |
      | test5 | Beedrill  |


