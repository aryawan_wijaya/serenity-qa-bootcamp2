@FormDataTable
Feature: fill form

  @FirstTryDataTable
  Scenario Outline: fill input form in seleniumeasy with '<test>'
    Given user open form
    Then validated url with "https://www.seleniumeasy.com/test/input-form-demo.html"
    When user fill the form with following data
      | firstName          | <firstName>   |
      | lastName           | <lastName>    |
      | email              | <email>       |
      | phone              | <phone>       |
      | address            | <address>     |
      | city               | <city>        |
      | state              | <state>       |
      | zipCode            | <zipCode>     |
      | website            | <website>     |
      | isHosting          | <hosting>     |
      | projectDescription | <projectDesc> |

    Examples:
      | test            | firstName | lastName | email             | phone | address | city  | state   | zipCode | website  | hosting | projectDesc  |
      | with hosting    | Blibli1   | Dot Com  | blibli@DotCom.Com | 081   | sentul1 | city1 | Georgia | 123     | asd.com  | true    | projectDesc1 |
      | without hosting | Blibli2   | Dot Com  | blibli@gdn.Com    | 082   | sentul2 | city2 | Florida | 1234    | asd2.com | false   | projectDesc2 |
      | Florida         | Blibli3   | Dot Com  | blibli@gdn.Com    | 083   | sentul3 | city3 | Florida | 12345   | asd3.com | true    | projectDesc3 |
      | outside florida | Blibli4   | Dot Com  | blibli@gdn.Com    | 084   | sentul4 | city4 | Arizona | 123456  | asd4.com | true    | projectDesc4 |

  @SecondTryDataTable
  Scenario Outline: fill data with scenario outline '<test>'
    Given user open form
    When user fill firstname with value '<firstName>'
    And user fill  lastname with value '<lastName>'
    And user fill  email with value '<email>'

    Examples:
      | test    | firstName | lastName | email             |
      | pertama | Blibli    | Dot Com  | blibli@DotCom.Com |
      | kedua   | Blibli    | .com     | blibli@gdn.Com    |