@APIUiPokemon
Feature: Pokemon Check
  @CekUiAPIPokemon
  Scenario Outline: compare between website and api --> <test> --> with pokemon <pokemon>
    Given user open website pokemonDb
    Then validated "<pokemon>" from website is display
    When user click "<pokemon>" from website
    And user save the data from website
    And user set "<pokemon>" api
    And user hit API
    Then compare pokemon from website and API

    Examples:
      | test  | pokemon   |
#      | test1 | weedle    |
      | test2 | Bulbasaur |
#      | test3 | Venusaur  |
#      | test4 | Raichu    |
#      | test5 | Beedrill  |
