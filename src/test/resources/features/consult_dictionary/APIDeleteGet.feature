@API
Feature: Test get and delete API
  @GetUserPositive
  Scenario: get singgle items from API
    Given [API] user set id get  1
    When  [API] user send request get
    Then  [API] status code get must be 200

  @DeleteUserPositive
  Scenario: delete data from API
    Given [API] user set id get  2
    When  [API] user send request delete
    Then  [API] status code delete must be 200