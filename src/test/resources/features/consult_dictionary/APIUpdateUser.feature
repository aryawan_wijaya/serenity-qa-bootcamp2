@API
Feature: Test update data
  @UpdateUserPositive
  Scenario: update with correct data
    Given   [API] user set id 2
    And     [API] user set email "asasaad@aa.com"
    And     [API] user set name "budi"
    And     [API] user set phone number "123456"
    When    [API] user send request
    Then    [API] response code must be 200
    And     [API] the data from user must equal with request
